package com.example.anil.bindingadapter_demo_103;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;

import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class UserViewModel extends BaseObservable {

    @Bindable
     String name ;

    @BindingAdapter("android:text")
    public static void getText(View view, String text) {

        Log.d("viewmodel", "getter called" + text);
        ((EditText)view).setText("hey "+text);
    }
    @BindingAdapter("android:afterTextChanged")
    public static void afterTextChanged(View view,Editable editable)
    {
        Log.d("viewmodel", "setter called" + editable);
        ((EditText)view).setText("hey "+editable);
    }

  /*  @Bindable
    public String getName() {
        Log.d("viewmodel", "getname called "+name);
        return name;
    }

    public void setName(String value) {

        Log.d("viewmodel", "setname called" + name);
        // Avoids infinite loops.
        if (name != value) {
            name = value;

            // React to the change.

            // Notify observers of a new value.
            notifyPropertyChanged(BR.name);
        }

    }*/

    public void onClick(View view)
    {
        //setName("hey ak");
    }



}