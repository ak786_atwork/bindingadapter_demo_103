package com.example.anil.bindingadapter_demo_103;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.example.anil.bindingadapter_demo_103.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        // Obtain the ViewModel component.
        UserViewModel userModel = new UserViewModel();

        // Inflate view and obtain an instance of the binding class.
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        // Assign the component to a property in the binding class.
        binding.setViewModel(userModel);

    }

}
